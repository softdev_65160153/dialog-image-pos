/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author Lenovo
 */
public class TestCustomerService {

    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        Customer cus1 = new Customer("Kob", "0999909999");
        cs.addNew(cus1);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0999909999");
        delCus.setTel("0111101111");
        cs.update(delCus);
        System.out.println("After Update");
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        
        cs.delete(delCus);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }        
        

    }
}

