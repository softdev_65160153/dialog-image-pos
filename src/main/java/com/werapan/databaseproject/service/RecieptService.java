/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.RecieptDao;
import com.werapan.databaseproject.dao.RecieptDetailDao;
import com.werapan.databaseproject.model.Reciept;
import com.werapan.databaseproject.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptService {
    public Reciept getById(int id){
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.get(id);
    }


    public List<Reciept> getReciepts() {
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.getAll(" reciept_id asc");
    }

    public Reciept addNew(Reciept editedReciept) {
        RecieptDao RecieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        Reciept reciept = RecieptDao.save(editedReciept);
        for(RecieptDetail rd: editedReciept.getRecieptDetails()) {
            rd.setRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public Reciept update(Reciept editedReciept) {
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.update(editedReciept);
    }

    public int delete(Reciept editedReciept) {
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.delete(editedReciept);
    }
}


